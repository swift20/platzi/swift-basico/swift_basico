import UIKit

//STORED PROPERTIES

struct FixedLengthRange {
    var firstValue : Int  // esta es un stored propertie
    let length : Int
}

var rangeOfThreeItems = FixedLengthRange(firstValue: 0, length: 3)
rangeOfThreeItems.firstValue = 6

let rangeOfFourItems = FixedLengthRange(firstValue: 0, length: 4)
//rangeOfFourItems.firstValue = 5 ESTO DA ERROR...


//Lazy Stored Properties, estas no inicializan su valor hasta que realmente se necesita en codigo

class DataImporter{
    var filename = "data.txt"
}

class DataManager { // se encarga de manejar todos los data importers que existen
    lazy var importer = DataImporter() // Esta variable solo es creada cuando es necesario
    var data = [String]()
}

let manager = DataManager()
manager.data.append("Algo de datos")
manager.data.append("Muchos más datos")
manager
//HAsta esta línea, el importer no ha sido creado...
manager.importer.filename
//Aquí, la variable importer ya ha sido creada
manager

//COMPUTED PROPERTIES

struct Point {
    var x = 0.0, y = 0.0
}

struct Size {
    var width = 0.0, height = 0.0
}

struct Rect {
    var origin = Point()
    var size = Size()
    var center: Point{
        get{
            Point(x: origin.x + size.width/2, y: origin.y + size.height/2)
        }
        set{
            origin.x = newValue.x - size.width/2
            origin.y = newValue.y - size.height/2
        }
    }
}

var square = Rect(origin: Point(x: 0, y: 0), size: Size(width: 10, height: 10))
square.center
let initialSquareCenter = square.center
square.center = Point(x: 18, y: 3)


//COMPUTED PROPERTIES DE SOLO LECTURA


struct Cuboid{
    var width = 0.0, height = 0.0, depth = 0.0
    var volume: Double{
        //Si es de solo lectura, no hay necesidad de colocar el get
        return width * height * depth
    }
}

let cuboid = Cuboid(width: 4.0, height: 5.0, depth: 2.0)
cuboid.volume
//cuboid.volume = 57.0 error, puesto que la variable es de solo lectura


//PROPERTY OBSERVERS


// willSet, Presencia e futuro,se llama justo antes de cambiar el valor de una property
// didSet, Presencia de pasado, se ejecuta despues de cambiar el valor de dicha property

class StepCounter{
    var totalSteps: Int = 0{
        willSet(newTotalSteps){ //mirar cambios antes de que ocurran
            print("El número de pasos va a subir hasta \(newTotalSteps)")
        }
        didSet{
            if totalSteps > oldValue{ //mirar cambios despues de que ocurran
                print("El número de pasos ha incrementado en \(totalSteps - oldValue)")
            }
        }
    }
}

let stepCounter = StepCounter()
stepCounter.totalSteps = 200

stepCounter.totalSteps = 520

stepCounter.totalSteps += 1234



//TYPE PROPERTIES
// static, una variable comun a todos los objetos, que sean instanciados a esa clase,
// static, esta es compartida por toda la clase, struct, enum

struct SomeStruct {
    var counter = 0
    static var storedTypeProperty = "SOME VALUE"
    static var computedTypeProperty: Int {
        return 1
    }
}

var instanceStr = SomeStruct()

var otherInstanceStr = SomeStruct()
// static, no se puede acceder directamnete por instancias, solo se puede acceder a travs de la propia estructura

SomeStruct.computedTypeProperty

enum SomeEnum{
    static var storedTypeProperty = "SomeValue"
    static var computedTypeProperty: Int{
        return 5
    }
}

SomeEnum.storedTypeProperty

class SomeClass{
    static var storedTypeProperty = "Some Value"
    static var computedTypeProperty:Int{
        return -9
    }
    // las subclases que quiero que sean static, debo ponerles class
    class var overrideableComputedTypeProperty:Int{
        return 108
    }
}
