import UIKit

//Herencia

class Vehicle {
    
    var currentSpeed = 0.0
    var description: String{
        return "Viajando a \(currentSpeed) km/h"
    }
    func makeNoise(){
        //do nothing, porque cada vehículo tiene su propia forma de hacer ruido
        print("El ruido depende del vehículo")
    }
}

let someVehicle = Vehicle()
print(someVehicle.description)


class Bicycle : Vehicle {
    var hasBasket = false
}

let bicycle = Bicycle()
bicycle.hasBasket = true
bicycle.currentSpeed = 15.0

print(bicycle.description)

class Tandem : Bicycle{
    var currentNumberOfPassengers = 0
}

let tandem = Tandem()
tandem.hasBasket = true
tandem.currentNumberOfPassengers = 2
tandem.currentSpeed = 22.0
print(tandem.description)


//Sobreescritura de variables y métodos

class Train: Vehicle{
    final var numberOfWagons = 0
    
    override func makeNoise() { // override es sobreescribir, cualquier cosa que hizo el padre, ahora me da igual, mi codigo es el que se ejecuta y no el del padre
       print("Chooo Chooo")
    }
}

let train = Train()
train.makeNoise()

tandem.makeNoise()


class Car: Vehicle{
    var gear = 1
    override var description: String{
        return super.description + " en la marcha \(gear)" // super trae lo que description tenia en la clase padre
    }
}

let car = Car()
car.currentSpeed = 55
car.gear = 3
print(car.description)
print(tandem.description)



class AutomaticCar : Car{
    override var currentSpeed: Double{
        didSet{ //observo cambios despues de que ocurren
            gear = Int(currentSpeed / 15.0) + 1
        }
    }
}

let automatic = AutomaticCar()
automatic.currentSpeed = 65
print(automatic.description)

class Railway : Train{

}


//con "final" evitamos que un metedo, propiedad o incluso un subindice sea sobreescrito por cualquiera de las clases hijos
