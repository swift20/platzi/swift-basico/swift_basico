import UIKit

//STRINGS

let someString = "Soy un string cualquiera"

let multiLineString = """
 Soy Juan Gabriel Gomila\
 Estamos haciendo el curso de Swift\
 Un saludo, paz y amor...
"""

print(multiLineString)


let wiseWords = "\"La imaginación es más importante que el saber\" - Albert Einstein"
let dolarSign = "\u{24}" //La "u" es para usar caracter unicode
let blackHeart = "\u{2665}"
let heart = "\u{1F496}"

//Inicilización y mutabilidad

var emptyString = ""
var anotherEmptyString = String()

if emptyString.isEmpty {
    print("Nada que ver aquí")
} else{
    print("El string tiene un valor")
}

var newSomeString = "Un caballo"
newSomeString += " y un carruaje"
newSomeString += " y un soldado"

let aString = "Juan Gabriel"
//aString += " y Ricardo Celis"

var a = "A"
var b = "B"
print("a vale \(a) y b vale \(b) ")
b = a
print("a vale \(a) y b vale \(b) ")
b = "C"
print("a vale \(a) y b vale \(b) ")
a = "D"
print("a vale \(a) y b vale \(b) ")

// Character

let name = "Juan Gabriel 😎"
for ch in name {
    print(ch)
}
print(name.count)

let exclamationMark : Character = "!"

let nameChars: [Character] = ["J", "u", "a", "n"]
var nameString = String(nameChars)

let compoundName = "Juan " + "Gabriel"

nameString.append(exclamationMark)

let multiplier = 3
var message = "El producto de \(multiplier) x 3.5 da \(Double(multiplier)*3.5)"
message.append(exclamationMark)

//INDICES DE STRINGS



let greeting = "Hola, ¿que tal?"
greeting[greeting.startIndex]
//greeting[greeting.endIndex] // es ultima posicion , como se sale del rango no podemos ingresar
greeting[greeting.index(before: greeting.endIndex)] // antes de endIndex
greeting[greeting.index(after: greeting.startIndex)] //despues de starIndex

for idx in greeting.indices{
    print("\(greeting[idx])", terminator:"")
}


var welcome = "Hola"

welcome.insert("!", at: welcome.endIndex)
welcome.insert(contentsOf: " que tal",
               at: welcome.index(before: welcome.endIndex))

welcome.remove(at: welcome.index(before: welcome.endIndex))
welcome
let range = welcome.index(welcome.endIndex, offsetBy: -7)..<welcome.endIndex
welcome.removeSubrange(range)
welcome

//SUBSTRINGS

greeting
let index = greeting.firstIndex(of: ",") ?? greeting.endIndex // indica la posicion de la primera coma , y sino tiene indica la posicion final
let firstPart = greeting[..<index]

let newString = String(firstPart)

//PREFIJO Y SUFIJO

let newGreeting = "Hola, soy Juan Gabriel"
newGreeting.hasPrefix("Hola") // Pregunta si inicia con la palabra "Hola"
newGreeting.hasSuffix("l") //Pregunta si finaliza con "l"

let collection = [
    "Act 1 Scene 1","Act 1 Scene 2","Act 1 Scene 3","Act 1 Scene 4","Act 1 Scene 5",
    "Act 2 Scene 1","Act 2 Scene 2","Act 2 Scene 3",
    "Act 3 Scene 1","Act 3 Scene 2"
]

var act1SceneCount = 0 // inicializamos nuestro contador
for scene in collection{ // Recorre collection
    if scene.hasPrefix("Act 1"){ // busca los que inicien con "Act 1"
        act1SceneCount += 1 // aumentamos cuando se cumpla el valor
    }
}

print("El número de escenas del acto 1 es: \(act1SceneCount)")

//Representacions Unicode

let ghost = "¡¡Fantasma!! 👻"

for codeUnit in ghost.utf8{
    print(codeUnit, terminator: " ")
}

print(" ")
for codeUnit in ghost.utf16{
    print(codeUnit, terminator: " ")
}

print(" ")
for codeUnit in ghost.unicodeScalars{
    print(codeUnit, terminator: " ")
}

