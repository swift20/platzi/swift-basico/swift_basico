import UIKit

//Struct vs Class

struct SomeStruct {
    //La definición de la estructura aquí
}
class SomeClass {
    //La definición de la clase aquí
}
struct Resolution {
    var width = 0
    var height = 0
}

class VideoMode {
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name: String?
}

let someResolution = Resolution()
let someVideoMode = VideoMode()

print(someResolution.width)
someVideoMode.resolution.width = 1280
print(someVideoMode.resolution.width)

someVideoMode.frameRate = 30.0
print(someVideoMode.frameRate)

//Una struct una ves creada, ocupa un espacio en memoria, y si la declaro como constante, esta permanece inmutable,por eso no la puedo modificar
//En el caso de un objeto de una clase, lo que tengo no es el propio objeto, sino la zona de la memoria que ocupa, las variables que tengo podran ir cambiado (solo si estan con var, y no como let)


//ESTRUCTURAS: DATOS COPIADOS POR VALOR


let vga = Resolution(width: 640, height: 480)
vga.width
vga.height

let hd = Resolution(width: 1920, height: 1080)

var cinema = hd
print("\(cinema.width) x \(cinema.height)")
cinema.width = 2048
print("\(cinema.width) x \(cinema.height)")
print("\(hd.width) x \(hd.height)")


enum CompassPoint{
    case north, south, east, west
}

var currentDirection = CompassPoint.north
var oldDirection = currentDirection
currentDirection = .south

print(currentDirection)
print(oldDirection)

//Estructuras y Enum, copian sus valores
//Clases, son datos referenciados, si de dos objetos uno es referenciado, al modificar uno de los dos, ambos se modifican

let tenEighty = VideoMode()
tenEighty.resolution = hd
tenEighty.interlaced = true
tenEighty.name = "1080i"
tenEighty.frameRate = 25.0

let alsoTenEighty = tenEighty
alsoTenEighty.frameRate = 30.0
tenEighty

//Los objetos de una clase, se comparan con "===", se quiere observar que las variables de uno de lo obsjetos y de la otra son las mismas
if tenEighty === alsoTenEighty{ //!==
    print("Son el mismo objeto")
}else{
    print("Son diferentes")
}
