import UIKit

//METODOS DE INSTANCIA

class Counter {
    var count = 0
    
    func increment(){
        self.count += 1 //propiedad de la propia clase
    }
    
    func increment(by amount:Int) {
        self.count += amount
    }
    
    func reset(){
        self.count = 0
    }
}

let counter = Counter()
counter.increment()
counter.increment(by: 8)
counter.reset()

//MUTATING METHODS

struct Point{
    var x = 0.0, y = 0.0
    func isToTheRight(of x:Double) -> Bool {
        return self.x > x
    }
    
    
    //Una funcion no puede cambiar los valores de una estrcutura,enumerado o clase, excepto si colocamos "mutating", esto indica que la funciona tiene el derecho de acceder a las propias variables, de la estructura o enumerado, y modificarlos si asi lo desea
    mutating func moveBy(x deltaX:Double, y deltaY: Double) {
        //self.x += deltaX
        //self.y += deltaY
        self = Point(x: self.x + deltaX, y: self.y + deltaY)
    }
}

var somePoint = Point(x: 4, y: 5)
somePoint.isToTheRight(of: 1)
somePoint.isToTheRight(of: 54)


somePoint.moveBy(x: 2, y: -3)
somePoint.x = 9


enum DifferentStateSwitch{
    case off, low, high
    mutating func next(){
        switch self {
        case .off:
            self = .low
        case .low:
            self = .high
        case .high:
            self = .off
        }
    }
}

var controllerStatus = DifferentStateSwitch.off
controllerStatus.next()
controllerStatus.next()
controllerStatus.next()

//METODOS DE CLASE

//Recordar // static, no se puede acceder directamnete por instancias, solo se puede acceder a travs de la propia estructura
class SomeClass{
    class func someMethod(){ // puede ser class o static, si m interesa la herencia es class
        print("HOLA")
    }
}

SomeClass.someMethod()


struct LevelTracker{
    static var highestUnlockedLevel = 1
    var currentLevel = 1
    
    static func unlock(_ level:Int){ // aca se uso static, porque no se deberia usar class, eso solo va dentro de class
        if level > highestUnlockedLevel{
            highestUnlockedLevel = level
        }
    }
    
    static func isUnlocked(_ level: Int) -> Bool{
        return level <= highestUnlockedLevel
    }
    
    mutating func advance(to level:Int) -> Bool {
        if LevelTracker.isUnlocked(level){
            currentLevel = level
            return true
        }else{
            return false
        }
    }
    
}


class Player{
    var tracker = LevelTracker()
    let playerName:String
    func complete(level: Int){
        LevelTracker.unlock(level + 1)
        tracker.advance(to: level + 1)
    }
    
    init(name: String) { //Inicializador de valores
        self.playerName = name
    }
}


var player = Player(name: "Juan Gabriel")
player.complete(level: 1)
player

player.complete(level: 7)

if player.tracker.advance(to: 7){
    print("Podemos avanzar hasta el nivel 7")
} else {
    print("El nivel 7 sigue bloqueado por ahora")
}
