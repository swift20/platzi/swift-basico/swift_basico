import UIKit

//Enumerations clase vaga

enum SomeEnumeration{
    //aquí iria la definición del enumerado
}

enum CompassPoint: String { // ese String se agrego en raw values, y se uso para consultar north como si fuera un string
    case north
    case south
    case east
    case west
}

enum Planet: Int{
    case mercury = 1, venus, earth, mars, jupiter, saturn, uranus, neptune
} //el numero "1", se coloco para la clase de raw value, y todos los demas valores seguiran ese valor automaticamente, mercury=1, venus02 ...

var directionToGo = CompassPoint.east
directionToGo = .south

switch directionToGo {
case .north:
    print("Hay que ir al norte")
case .south:
    print("Hay pinguinos en el sur")
case .east:
    print("Mordor se extiende hacia las tierras del este")
case .west:
    print("Cuidado con los vaqueros")
}

let somePlanet = Planet.mars
switch somePlanet {
case .earth:
    print("La tierra es segura")
default:
    print("No es seguro ir a este planeta")
}

enum Beverage: CaseIterable { // Si queremos recorrer los elementos de un enumeration, tenemos que usar un protocolo llamado "CaseIterable", los protocolos permiten delegar funcionalidades
    case coffee, tea, juice, redbull
}
let numberOfChoices = Beverage.allCases.count
for beverage in Beverage.allCases{
    print(beverage)
}

//Enumerations, Ejemplo codigo de barras

enum Barcode{
    case upc(Int, Int, Int, Int)
    case qrCode(String)
}

var productBarcode = Barcode.upc(8, 85909, 51226, 3)
productBarcode = .qrCode("ASDFGHJKL")

switch productBarcode {
case let .upc(numberSystem, manufacturer, product, check):
    print("UPC: \(numberSystem), \(manufacturer), \(product), \(check).")
case let .qrCode(productCode):
    print("QR: \(productCode)")
}


//Enumerados con raw values

enum ASCIIControlCharacter: Character{
    case tab = "\t"
    case lineFeed = "\n"
    case carriageReturn = "\r"
}



let earthOrder = Planet.earth.rawValue // me da su valor raw, le dimos un 1 en planetas

let northDirection = CompassPoint.north.rawValue // en Compasspoint, no pusimos un valor

let possiblePlanet = Planet(rawValue: 5)

let planetPosition = 3
if let anyPlanet = Planet(rawValue: planetPosition){
    switch anyPlanet{
        case .earth:
            print("La tierra es segura")
        default:
            print("No es seguro ir a este planeta")
    }
}else{
    print("El planeta indicado no existe... ")
}
